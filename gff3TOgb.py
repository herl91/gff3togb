#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function
import sys
import gzip
import re

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import generic_dna, generic_protein
from Bio.SeqFeature import FeatureLocation, CompoundLocation, SeqFeature
from BCBio import GFF


class Options(object):
    gff_file = ''
    seq_file = ''
    out_file = ''
    multi_out = False
    allow_partials = False
    use_memory = False
    chr_filter = ''
    pep_file_in = ''
    pep_file_out = ''
    verbose = False
    single_entries = False


def print_help():
    print('''
usage: gff3TOgb.py gff3File seqFile outFile [options]

seqFile should be unzipped. To use gzipped seqFiles, add '-usememory' flag. Read what this means below.

Optional flags:
    --split         outFile must be a dir. The script will write one output per record in the given dir.
    --nopartials    The script will not output CDS with a 'partial' or 'pseudo' note.
    --usememory     This will make the script load the seqFile (and pepFileIn if applicable) entirely into memory.
                    This allows for gzipped seqFiles. Not advisable for big seqs or low memory machines.
    -v / --verbose
    -h
    
Optional input:
    --chr           Only given chr will be processed.
    --pepInput      Given a protein fasta file, the script will add information from it to the output.
    --pepOutput     Will produce a multifasta file with all translations and geneID as identifier.
''')
    return


def parse_input(argv):
    """
    Parses input and stores in Options object.
    
    Args:
        argv (iterable): 
    """
    Options.gff_file = sys.argv[1]
    Options.seq_file = sys.argv[2]
    Options.out_file = sys.argv[3]

    for i, arg in enumerate(argv):
        if arg == '-h':
            print_help()
            sys.exit()
        elif arg == '--split':
            Options.multi_out = True
        elif arg == '--nopartials':
            Options.allow_partials = False
        elif arg == '--usememory':
            Options.use_memory = True
        elif arg == '--chr':
            Options.chr_filter = argv[i+1]
        elif arg == '--pepInput':
            Options.pep_file_in = argv[i+1]
        elif arg == '--pepOutput':
            Options.pep_file_out = argv[i+1]
        elif arg == '-v' or arg == '--verbose':
            Options.verbose = True
    return


def check_gff_suitability(sdict):
    """
    Function will exit to terminal if gff is not suitable.
    
    Args:
        sdict (dict): 
    """
    seq_ids = list(sdict.keys())
    try:
        examiner = GFF.GFFExaminer()
        gff_data = examiner.available_limits(open(Options.gff_file))

        # Check if at least one GFF locus appears in sequence
        gff_ids = set([n[0] for n in gff_data['gff_id']])

        # If both inputs only have one record, assume is the same, but first check coordinate compatibility
        if len(gff_ids) == 1 and len(seq_ids) == 1:
            print("GFF3 and sequence have only one record. Assuming is the same as long as coordinates are compatible.")

            limit_info = dict(gff_type=['CDS'])
            for gff_record in GFF.parse(open(Options.gff_file), limit_info=limit_info):
                break

            gff_coord_max = max([n.location.end.real for n in gff_record.features])
            seq_coord_max = len(sdict[seq_ids[0]].seq)

            if gff_coord_max > seq_coord_max:
                print('GFF3 record and sequence coordinates are not compatible.')
                sys.exit(1)
            else:
                Options.single_entries = True

        elif len(gff_ids.intersection(set(seq_ids))) == 0:
            print('No GFF3 record IDs match any sequence record IDs.')
            sys.exit(1)

        else:
            Options.single_entries = False

        # Check GFF contains CDSs
        if not ('CDS',) in gff_data['gff_type']:
            print('GFF3 does not contain any CDS.')
            sys.exit(1)

        # Check CDS are childless but not parentless
        if 'CDS' in set([n for key in examiner.parent_child_map(open(Options.gff_file)) for n in key]):
            print('GFF3 structure is not suitable. CDS features must be childless but not parentless.')
            sys.exit(1)

    except AssertionError as e:
        print('Parsing', Options.gff_file, 'failed:', e)
        sys.exit(1)

    return


def check_sub(feature, fasta, match):
    """
    Tree explorer.
    
    Args:
        feature: 
        fasta: 
        match: 

    Returns:
        : new_feature
    """
    new_feature = []
    loc_list = []
    qual_list = {}
    topop = []
    for sub in feature.sub_features:
        if sub.sub_features:  # If there are sub_features, go deeper
            new_feature.append(check_sub(sub, fasta, match))
        elif sub.type == match:
            if sub.type == 'CDS':
                loc = [sub.location.start.real, sub.location.end.real]
                loc_list.append(FeatureLocation(loc[0], loc[1], strand=sub.strand))
            else:
                loc_list.append(sub.location)
            # For split features (CDSs), the final feature will have the same qualifiers as the children ONLY if
            # they're the same, i.e.: all children have the same "protein_ID" (key and value).
            for qual in sub.qualifiers.keys():
                if not qual in qual_list:
                    qual_list[qual] = sub.qualifiers[qual]
                if qual in qual_list and not qual_list[qual] == sub.qualifiers[qual]:
                    topop.append(qual)

    for n in topop:  # Pop mismatching qualifers over split features
        qual_list.pop(n, None)
    qual_list.pop('Parent', None)  # Pop parent.

    # Only works in tip of the tree, when there's no new_feature built yet. If there is,
    # it means the script just came out of a checkSub and it's ready to return.
    if not new_feature:
        if len(loc_list) > 1:
            loc_list = sorted(loc_list, key=lambda x: x.start.real)
            if loc_list[0].strand == 1:
                new_loc = CompoundLocation(loc_list)
            else:
                new_loc = CompoundLocation(list(reversed(loc_list)))
        elif len(loc_list) == 0:
            return new_feature
        else:
            new_loc = loc_list[0]

        new_feature = SeqFeature(new_loc)
        new_feature.qualifiers = qual_list

        if match == 'CDS':
            new_feature.type = match
            trans = new_feature.extract(fasta).translate(stop_symbol='')
            new_feature.qualifiers['translation'] = [trans]
        else:
            new_feature.type = 'mRNA'

    return new_feature


def main(argv):
    if len(sys.argv) < 4:
        print('\nNot enough arguments.')
        print_help()
        sys.exit()

    parse_input(argv)


    ################################################################## LOAD INPUT
    # Seq File
    sdict = {}
    if not Options.use_memory:
        if '.gz' in Options.seq_file:
            print("seqFile looks gzipped. Unzip or add '--usememory' flag.")
            sys.exit()
        if Options.verbose:
            print('Indexing seqFile...')
        sdict = SeqIO.index(Options.seq_file, 'fasta', alphabet=generic_dna)
    else:
        if Options.verbose:
            print('Loading seqFile...')
        if '.gz' in Options.seq_file:
            handle = gzip.open(Options.seq_file)
        else:
            handle = open(Options.seq_file, "rU")
        sdict = SeqIO.to_dict(SeqIO.parse(handle, 'fasta', alphabet=generic_dna))
    if Options.verbose:
        print('Done!')

    # Pep File Input
    if Options.pep_file_in:
        if not Options.use_memory:
            if Options.verbose:
                print('Indexing pepFileIn...')
            pdict = SeqIO.index(Options.pep_file_in, 'fasta', alphabet=generic_protein)
        else:
            if Options.verbose:
                print('Loading pepFileIn...')
            if '.gz' in Options.pep_file_in:
                handle = gzip.open(Options.pep_file_in)
            else:
                handle = open(Options.pep_file_in, "rU")
            pdict = SeqIO.to_dict(SeqIO.parse(handle, 'fasta', alphabet=generic_protein))
        if Options.verbose:
            print('Done!')
    ##################################################################

    if '.gz' not in Options.gff_file:
        if Options.verbose:
            print('Checking gff suitability...')
        check_gff_suitability(sdict)
        if Options.verbose:
            print('Done!')

    # Read GFF
    if '.gz' in Options.gff_file:
        handle = gzip.open(Options.gff_file)
    else:
        handle = open(Options.gff_file, "rU")
    r = []
    if Options.pep_file_out:
        p = []

    if Options.verbose:
        print('Reading GFF...')
    for record in GFF.parse(handle, target_lines=100):
        sid = record.id
        # If there's a chrfilter, check and ignore if this isn't it.
        if Options.chr_filter and not record.id == Options.chr_filter:
            continue
        if record.id not in sdict:  # Any record that is not in the seq_file will be ignored.
            continue

        # Load seq to memory so index doesn't have to be accessed everytime we're extracting for e.g. translations.
        sidseq = sdict[sid].seq
        r.append(SeqRecord(sidseq, id=record.id))  # Add the level 1 record (chr/sca/contig), to r.
        for feature in record.features:  # Delve deeper into the tree
            if not Options.allow_partials:
                if ('Note' in feature.qualifiers and
                        ('partial' in feature.qualifiers['Note'][0] or 'pseudo' in feature.qualifiers['Note'][0])):
                    continue

            # Append first level
            if feature.type == 'gene':
                r[-1].features.append(feature)
            elif feature.type == 'CDS':
                trans = feature.location.extract(sidseq).translate(stop_symbol='')._data
                feature.qualifiers['translation'] = trans
                if 'gene' not in feature.qualifiers:
                    feature.qualifiers['gene'] = feature.qualifiers['ID'][0]
                r[-1].features.append(feature)
                if Options.pep_file_out:
                    p.append(SeqRecord(Seq(trans,
                                           alphabet=generic_protein), id=feature.qualifiers['gene'], description=''))
            ####################

            for match in ['exon', 'CDS']:
                new_feature = check_sub(feature, sidseq, match)
                if not new_feature:
                    continue
                if not type(new_feature) == list:
                    new_feature = [new_feature]
                new_feature = filter(None, new_feature)
                for n in new_feature:
                    n.qualifiers['gene'] = feature.id
                    if 'protein_id' in n.qualifiers:
                        pid = n.qualifiers['protein_id'][0]
                        if Options.pep_file_in and pid in pdict:
                            n.qualifiers['note'] = pdict[pid].description
                    r[-1].features.append(n)
                    if Options.pep_file_out and match == 'CDS':
                        p.append(SeqRecord(Seq(n.qualifiers['translation'][0]._data,
                                               alphabet=generic_protein), id=n.qualifiers['gene'], description=''))
    handle.close()
    del record, sidseq, sdict

    if Options.verbose:
        print('Done!\nWriting...')

    if Options.pep_file_out:
        SeqIO.write(p, Options.pep_file_out, 'fasta')

    if not Options.multi_out:
        SeqIO.write(r, Options.out_file, 'genbank')
    else:
        for n in r:
            name = Options.out_file + n.id + '.gbk'
            SeqIO.write(n, name, 'genbank')
    if Options.verbose:
        print('Done!')

    return


if __name__ == "__main__":
    main(sys.argv[1:])
