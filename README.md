# WARNING #
This code is quite complete but there may be errors still.


# INSTALLATION #
clone repo and do:

    pip install -r requirements.txt


# USAGE #
    gff3TOgb.py gff3File seqFile outFile [options]



# NOTES #
seqFile should be unzipped. To use gzipped seqFiles, add '-usememory' flag. Read what this means below.


# OPTIONAL FLAGS #
    --split         outFile must be a dir. The script will write one output per record in the given dir.
    --nopartials    The script will not output CDS with a 'partial' or 'pseudo' note.
    --usememory     This will make the script load the seqFile (and pepFileIn if applicable) entirely into memory.
                    This allows for gzipped seqFiles. Not advisable for big seqs or low memory machines.
    -v / --verbose
    -h


# OPTIONAL INPUT #
    --chr           Only given chr will be processed.
    --pepInput      Given a protein fasta file, the script will add information from it to the output.
    --pepOutput     Will produce a multifasta file with all translations and geneID as identifier.

# EXAMPLE #
    gff3TOgb.py my_gff_file.gff3 my_fasta_file.fa my_gbk_output.gbk --pepOutput my_proteins.fa --usememory